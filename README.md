# MTL Scheduler #

MTL Scheduler transforms general purpose scheduling problems specified using Metric Temporal Logic (MTL) into first order logic and find solutions using SMT solver.

The scheduler takes as input a ```.smtl``` model. A SMTL model consists of four parts,

1. Time range: the bounded horizon of scheduling problem domain;
2. Actions: the set of possible activities to schedule;
3. Properties: the property functions defined over Actions;
4. Constraints: the set of constraints for the schedule written in MTL.

Here is a simple SMTL model for the classic job shop scheduling problem:

```
::python
# schedule time range upper bound
begin 0 final 21

# action declarations: 3 jobs on 2 machines
ACTIONS
{
  a1_1, a1_2 = 1 # Actions for job 1
  a2_1, a2_2 = 1 # Actions for job 2
  a3_1, a3_2 = 1 # Actions for job 3
}

# property definitions
PROPERTIES
{
  # Which actions correspond to which machine
  U_1: a1_1, a2_1, a3_1
  U_2: a1_2, a2_2, a3_2
}

# MTL constraints
CONSTRAINTS
# duration of each job on each machine
Duration(a1_1,13); Duration(a2_1,2); Duration(a3_1,1); 
Duration(a1_2,3); Duration(a2_2,5); Duration(a3_2,3);

# One job on machine 1 at any time
(Forall_sym i:U_1, j:U_1 . G !(Currently(i,true) & Currently(j,true)));

# One job on machine 2 at any time
(Forall_sym i:U_2, j:U_2 . G !(Currently(i,true) & Currently(j,true)));

# Ordering of parts of each job (Machine 1 before 2)
Order(a1_1, a1_2); Order(a2_1, a2_2); Order(a3_1, a3_2);
```

### Dependencies ###
* Z3: [https://github.com/Z3Prover/z3](https://github.com/Z3Prover/z3)
* pyPEG2: ```pip install pypeg2```
* nose: ```pip install nose```

### Setup ###
```sudo ./setup install```

### Run Scheduler ###
```./bin/scheduler```

For detailed usage of the tool, please use the ```-help``` option and refer to documentations.

### Run Tests ###
Run default tests using nose: ```nosetests```

See example input files in ```tests/*.smtl```

### Publications ###
[Using Metric Temporal Logic to Specify Scheduling Problems](http://www.cs.toronto.edu/~liyi/host/files/kr16.pdf)   
Roy Luo, Richard Valenzano, Yi Li, Christopher Beck and Sheila McIlraith   
In proceedings of the 15th International Conference on Principles of Knowledge Representation and Reasoning (KR 2016)