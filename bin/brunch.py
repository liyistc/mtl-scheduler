#!/usr/bin/env python

import sys
import os
import os.path
import subprocess as sub
import csv

rootdir = os.path.dirname(os.path.realpath(__file__))
scheduler = "/home/liyi/bit/mtl-scheduler/bin/scheduler"

def isexec(fpath):
    if fpath == None:
        return False
    return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

def collectStats (stats, filename):
    with open(filename, 'r') as f:
        stats['Result'] = 'solved'
        for line in f:
            if line.startswith('Solution not found!'):
                stats['Result'] = 'no solution'

            if not line.startswith ('Time elapsed:'):
                continue
            
            fld = line.split (' ')
            stats['Solve'] = '{:.2f}'.format(float(fld[2].strip()))

    return stats

def statsHeader(stats_file, flds):
    with open(stats_file, 'w') as sf:
        writer = csv.writer(sf)
        writer.writerow (flds)

def statsLine(stats_file, fmt, stats):
    line = list()
    for fld in fmt:
        if fld in stats:
            line.append(str(stats[fld]))
        else:
            line.append(None)

    with open(stats_file, 'a') as sf:
        writer = csv.writer(sf)
        writer.writerow(line)


def brunch(bench, mem, cpu):
    import datetime as dt
    import resource as r

    def set_limits():
        if mem > 0:
            mem_bytes = mem * 1024 * 1024
            r.setrlimit(r.RLIMIT_AS, [mem_bytes, mem_bytes])
        if cpu > 0:
            r.setrlimit(r.RLIMIT_CPU, [cpu, cpu])

    bench_name = os.path.basename(bench)
    dt = dt.datetime.now().strftime('%d_%m_%Y-t%H-%M-%S')
    out = os.path.join(".", "out.{b}.date-{dt}".format(b=bench_name,dt=dt))
    os.mkdir(out)
    fmt = ['File', 'Status', 'Result', 'Cpu', 'Solve']
    statsHeader(os.path.join(out, 'stats'), fmt)
    cpuTotal = r.getrusage (r.RUSAGE_CHILDREN).ru_utime

    for root, subdirs, files in os.walk(os.path.join(rootdir, bench)):
        for filename in files:
            print os.path.join(root, filename)
            brunch_args = [scheduler,\
                           os.path.join(root, filename)]

            outfile = os.path.join(out, filename + '.stdout')
            errfile = os.path.join(out, filename + '.stderr')

            p = sub.Popen(brunch_args, stdout=open(outfile, 'w'), \
                          stderr=open(errfile, 'w'), \
                          preexec_fn = set_limits)
            p.wait()
            cpuUsage = r.getrusage (r.RUSAGE_CHILDREN).ru_utime

            stats = dict()
            stats['File'] = filename
            stats['Status'] = p.returncode
            stats['Cpu'] = '{:.2f}'.format (cpuUsage - cpuTotal)
            cpuTotal = cpuUsage

            stats = collectStats (stats, outfile)
            stats = collectStats (stats, errfile)
            statsLine (os.path.join(out, 'stats'), fmt, stats)

def main():
    import argparse

    parser = argparse.ArgumentParser(description = 'Bench Runner')
    parser.add_argument('bench', metavar='BENCH', \
                        help = 'Path to the benchmark file')
    parser.add_argument('--cpu', metavar='CPU', \
                        type=int, help='CPU limit', default=1000)
    parser.add_argument('--mem', metavar='MEM', \
                        type=int, help='Memory limit (MB)', default=1024)
    parser.add_argument('--out', metavar='DIR', \
                        default='brunch.out', help='Output directory')

    args = parser.parse_args()

    brunch(bench=args.bench, mem=args.mem, cpu=args.cpu)

if __name__ == '__main__':
    sys.exit(main())
