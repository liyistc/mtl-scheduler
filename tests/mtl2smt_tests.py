# Test cases for parser and converter
#
# Author: Yi Li

from nose.tools import *
from mtl2smt import *
from mtl2smt.parser import *
import os

from z3 import *

def setup():
    print "SETUP!"

def teardown():
    print "TEAR DOWN!"


class PrintVisitor(object):
    def __init__(self, visited):
        visited.accept(self)

    def visit(self, data_obj):
        if isinstance(data_obj, Action):
            print data_obj.begin


def test_parser():
    print "Parse!"
    formula = mtl_parse("((Currently(n1,true) | Currently(n2,true) | Currently(n3,true) | Currently(n4,true) | Currently(n5,true)) U[0,5] Currently(n5, true))")
    print formula
    #visitor = PrintVisitor(formula)
    #visitor.visit(formula)
    rw = SymbolRewriter({'n1':'N1','n2':'N2','true':'false'})
    rw.rewrite(formula)
    for i in formula[0].left[0]:
        print i[0].arg


script_dir = os.path.dirname(__file__)


def test_jobshop():
    print "Jobshop!"
    compiler = MTLCompiler(os.path.join(script_dir, "air.smtl"))
    compiler.compiles()
    compiler = MTLCompiler(os.path.join(script_dir, "forall.smtl"))
    compiler.compiles()
    
    #visitor = PrintVisitor(compiler.model)
    #visitor.visit(compiler.model)

def test_nurse():
    print "Nurse!"
    compiler = MTLCompiler(os.path.join(script_dir, "bound.smtl"))
    smt = compiler.compiles()
    compiler = MTLCompiler(os.path.join(script_dir, "nurse.smtl"))
    smt = compiler.compiles()
