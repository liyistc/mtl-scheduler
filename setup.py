#!/usr/bin/python
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'MTL Scheduler',
    'author': 'Yi Li',
    'url': 'https://bitbucket.org/liyistc/mtl-scheduler',
    'download_url': 'https://bitbucket.org/liyistc/mtl-scheduler',
    'author_email': 'liyi@cs.toronto.edu',
    'version': '0.3',
    'install_requires': ['nose','pypeg2'],
    'packages': ['mtl2smt'],
    'scripts': [],
    'name': 'mtl-scheduler'
}

setup(**config)
