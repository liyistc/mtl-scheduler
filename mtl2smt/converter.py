# MTL Scheduler: a general purpose scheduling tool.
# Copyright (C) 2015  Yi Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from z3 import *
from mtl2smt.parser import *
from collections import defaultdict
from mtl2smt.utils import *
from copy import deepcopy
import itertools

actSort = DeclareSort('Act')
start = Function('start', actSort, IntSort())
end = Function('end', actSort, IntSort())

qe = Tactic('qe')
enableQE = True


class MTLCompiler:
    # MTL to SMT converter (based on Roy Luo's code).
    def __init__(self, file_path, print_encoding=False):
        self.model = mtl_parse_file(file_path)
        self.action_bound = dict() # bound for action instances
        self.action_z3 = dict() # str -> z3 object
        self.property_map = defaultdict(list) # p -> p[a1,a2,...,an]
        self.solver = Solver()
        self.print_encoding = print_encoding
        
    def compiles(self):
        self.genTimeRange()
        self.genActions()        
        self.genProperties()        
        self.genConstraints()
        self.execute()
            
        return None

    def genTimeRange(self):
        # codegen TimeRange
        schedule_range = self.model[0]
        assert type(schedule_range) is TimeRange
        self.schedule_begin = int(schedule_range.begin) # schedule begin time
        self.schedule_final = int(schedule_range.final) # schedule end time
        assert self.schedule_begin <= self.schedule_final, \
            "Schedule time range is invalid!"
    
    def genActions(self):
        # codegen Actions
        actions = self.model[1]
        assert type(actions) is Actions

        for action in actions:
            for a in action.items:
                # print a + ':' + action.bound
                bound = int(action.bound)
                assert bound > 0, "Invalid bound %d!" % bound
                self.action_bound[a] = bound
                
                if bound == 1:
                    self.action_z3[a] = Const(a, actSort)
                else:
                    for i in range(0, bound):
                        flatten = '__A__'+a+'_'+str(i)
                        self.action_z3[flatten] = Const(flatten, actSort)
                        self.property_map[a].append(flatten)

                        
    def genProperties(self):
        # codegen Properties
        properties = self.model[2]
        assert type(properties) is Properties
        
        for p in properties:
                        
            for i in p.items:
                # print p.name + ":" + i
                self.property_map[p.name].append(i)
                assert i in self.action_z3, "Action %s is undefined!" % i
                
            
    def genConstraints(self):
        # codegen Constraints
        constraints = self.model[3]
        assert type(constraints) is Constraints
        convert = FormulaConverter(self.action_z3, self.property_map)

        # add range constraints
        act = Const('a', actSort)
        self.solver.add(ForAll(act, And(self.schedule_begin <= start(act), \
                                 self.schedule_final >= end(act))))

        # optimization: consolidate G and F
        # optimized_cons = convert.optimize(constraints)
        optimized_cons = constraints
        
        # add MTL constraints
        for f in optimized_cons:
            ast = convert.convert(f)
            self.solver.add(simplify(ast))
            
    def execute(self):
        # print asserted constraints
        if self.print_encoding:
            print self.solver.assertions()

        if self.solver.check() == sat:
            # print self.solver.model().sexpr()
            model = self.solver.model()

            # print start, end pairs
            for (a, z3_a) in self.action_z3.iteritems():
                print '[start(%s), end(%s)] = [%s, %s]' % \
                    (a, a, model.evaluate(start(z3_a)), \
                     model.evaluate(end(z3_a)))
        else:
            print "Solution not found!"
                

class SymbolRewriter:
    '''
    Rewrite formula by replacing symbols according to re-writing rules.
    '''    
    def __init__(self, rules={}):
        self.rules = rules

    class SymbolVisitor(object):
        def __init__(self, visited, rules):
            self.rules = rules
            visited.accept(self)
            
        def visit(self, data):
            if isinstance(data, SimpleArgs) or isinstance(data, FormulaArgs):
                for i in range(0, len(data)):
                    if isinstance(data[i], Identifier):
                        data[i] = Identifier(self.rewrite_symbol(data[i].name))

        def rewrite_symbol(self, symbol):
            if symbol in self.rules:
                # print 'rewriting %s to %s ...' % (symbol, self.rules[symbol])
                return self.rules[symbol]
            else:
                return symbol

    def rewrite(self, formula):
        visitor = self.SymbolVisitor(formula, self.rules)
        visitor.visit(formula)


class FormulaConverter:
    
    def __init__(self, action_map=dict(), property_map=defaultdict(list)):
        self.varCount = 0
        self.action_map = action_map
        self.property_map = property_map
        
    def freshInt(self):
        self.varCount = self.varCount + 1
        return Int('i_'+ str(self.varCount))

    def freshAct(self):
        self.varCount = self.varCount + 1
        return Const('a_' + str(self.varCount), actSort)

    def interval(self, lb, ub):
        interval = Interval()
        interval.lb = str(lb)
        interval.ub = str(ub)
        return interval
    
    def always(self, formula, lb='0', ub='+oo'):
        res = UnExp()
        res.operator = 'G'
        res.duration = self.interval(lb,ub)
        res.operand = formula
        return res
    
    def eventually(self, lb, ub, formula):
        res = UnExp()
        res.operator = 'F'
        res.duration = self.interval(lb,ub)
        res.operand = formula
        return res

    def conjunction(self, a, b):
        res = BinExp()
        res.operator = '&'
        res.left = a
        res.right = b
        return res

    def conjunction_list(self, forms):
        if len(forms) == 1:
            return forms[0]
        else:
            return self.conjunction(forms[0], self.conjunction_list(forms[1:]))

    def before(self, formula):
        argList = FormulaArgs()
        argList.append(formula)
        res = Precedence()
        res.func = 'Before'
        res.arg = argList
        return res

    def after(self, formula):
        argList = FormulaArgs()
        argList.append(formula)
        res = Precedence()
        res.func = 'After'
        res.arg = argList
        return res
        
    def start(self, a):
        argList = SimpleArgs()
        argList.append(a)
        res = Predicate()
        res.func = 'start'
        res.arg = argList
        return res
        
    def end(self, a):
        argList = SimpleArgs()
        argList.append(a)
        res = Predicate()
        res.func = 'end'
        res.arg = argList
        return res

    '''
    optimize encoding by grouping multiple G-constraints together
    '''
    def optimize(self, formulas):
        g_cons = []
        optimized_f = []
        conjunction = True
        
        for f in formulas:
            if type(f) is Formula and type(f[0]) is UnExp:
                if f[0].operator == 'G' and not hasattr(f[0], 'duration'):
                   g_cons.append(f[0].operand) 
                   continue
               
            optimized_f.append(f)
            

        optimized_f.append(self.always(self.conjunction_list(g_cons)))
        return optimized_f

    '''
    Apply symmetry reduction on re-writing rules.
    Three types of reduction:
    1. Forall: no reduction
    2. Forall_neq: reduce (a,a)
    3. Forall_sym: reduce (a,a) and (a,b)+(b,a)
    '''
    def ruleReduction(self, rules, quant='Forall'):
        res = []
        sigset = set()

        if quant == 'Forall':
            return rules
        
        for r in rules:
            if not len(r.keys()) == 2:
                return rules
            
            key0, key1 = r.keys()[0], r.keys()[1]
            
            # reduce (a,a)
            if r[key0] == r[key1]:
                continue
            # reduce (a,b), (b,a)
            if quant == 'Forall_sym' and \
               ((r[key0], r[key1]) in sigset or (r[key1], r[key0]) in sigset):
                continue
            
            sigset.add((r[key0],r[key1]))
            res.append(r)

        #print len(rules)
        #print len(res)

        return res
            
        
    def convert(self, formula_ast):
        return self.exactlyOnce(formula_ast, 0)
        
    def exactlyOnce(self, formula, x):
        # print("converting formula: " + str(formula))

        # true, false
        if type(formula) is Boolean:
            return formula == 'true'

        # predicate: start(a), end(a), P(a)
        if type(formula) is Predicate:
            func = formula.func
            arg = formula.arg
            
            if func == 'start' or func == 'end':
                # start(a), end(a)
                assert len(arg) == 1, \
                    "Number of arguments is incorrect: %s(%s)!" \
                    % (func, arg)
                # declare arg as a constant of sort 'Act'
                z3_arg = self.action_map[arg[0]]
            
                return start(z3_arg) == x if func == 'start' else \
                    end(z3_arg) == x
            
            elif func == 'Duration':
                # Duration(a, k)
                assert len(arg) == 2, \
                    "Number of arguments for Duration is incorrect: %s(%s)!" \
                    % (func, arg)
                z3_arg = self.action_map[arg[0]]
                assert representInt(arg[1]), "Invalid duration value!"
                return end(z3_arg) - start(z3_arg) == int(arg[1])

            elif func == 'Order':
                assert len(arg) >= 2, \
                    "Number of arguments for Order is incorrect: %s(%s)!" \
                    % (func, arg)
                z3_arglist = [self.action_map[i] for i in arg] 
                return And([end(a) <= start(b) \
                            for a, b in pairwise(z3_arglist)])
                
            else:
                # other properties P(a)
                assert len(arg) == 1, \
                    "Number of arguments for property is incorrect: %s(%s)!" \
                    % (func, arg)
                return arg[0] in self.property_map[func]

            return True

        if type(formula) is Precedence:
            func = formula.func
            arg = formula.arg
            # print func
            # print arg
            if func == 'Before' or func == 'After':
                assert len(arg) == 1, \
                    "Number of arguments is incorrect: %s(%s)!" & (func, arg)
                lb = '0' if func == 'Before' else '-oo'
                ub = '+oo' if func == 'Before' else '0'
                return self.exactlyOnce(self.eventually(lb, ub, arg[0]), x)

            elif func == 'Between':
                assert len(arg) == 2, \
                    "Number of arguments is incorrect: %s(%s)!" & (func, arg)
                return And(self.exactlyOnce(self.after(arg[0]), x), \
                           self.exactlyOnce(self.before(arg[1]), x))

            elif func == 'Currently':
               assert len(arg) == 2, \
                    "Number of arguments is incorrect: %s(%s)!" & (func, arg)
               return And(self.exactlyOnce(self.after(self.start(arg[0])), x), \
                          self.exactlyOnce(self.before(self.end(arg[0])), x))

            else:
                assert False, "Operator %s not supported!" % func
               
            return True

        # unary expression: G[l,u] formula, F[l,u] formula, ! formula
        if type(formula) is UnExp:
            op = formula.operator
            term = formula.operand
            
            i = self.freshInt()
            
            if op == 'G':

                if not hasattr(formula, 'duration'):
                    lb = '0'
                    ub = '+oo'
                else:
                    lb, ub = (formula.duration.lb, \
                              formula.duration.ub)

                fact = Implies(closeRangeForm(i, lb, ub), self.exactlyOnce(term, i + x))
                            
                return ForAll(i, fact)

            elif op == 'F':
                if not hasattr(formula, 'duration'):
                    lb = '0'
                    ub = '+oo'
                else:
                    lb, ub = (formula.duration.lb, \
                              formula.duration.ub)
               
                fact = And(closeRangeForm(i, lb, ub), self.exactlyOnce(term, i + x))

                return Exists(i, fact)
                

            elif op == '!':
                return Not(self.exactlyOnce(term, x))

        if type(formula) is NaryAnd:
            return And([self.exactlyOnce(i, x) for i in formula])

        if type(formula) is NaryOr:
            return Or([self.exactlyOnce(i, x) for i in formula])
            
        # binary expression: formula U[l,u] formula, formula & formula, formula -> formula
        if type(formula) is BinExp:
            op = formula.operator
            left = formula.left
            right = formula.right

            if op == '=>':
                return Implies(self.exactlyOnce(left, x), self.exactlyOnce(right, x))

            
            elif op == 'U':
                
                if not hasattr(formula, 'duration'):
                    lb = '0'
                    ub = '+oo'
                else:
                    lb, ub = (formula.duration.lb, \
                              formula.duration.ub)

                #assert lb <= ub, 'Range [%d, %d] is invalid!' % (lb, ub)
                
                i = self.freshInt()
                j = self.freshInt()
                
                if ub <= 0:
                    return Exists(i, And(closeRangeForm(i, lb, ub), self.exactlyOnce(right, i+x), \
                                         ForAll(j, Implies(And(i <= j, j <= 0), \
                                                           self.exactlyOnce(left, j+x)))))
                elif lb >= 0:
                    return Exists(i, And(closeRangeForm(i, lb, ub), self.exactlyOnce(right, i+x), \
                                         ForAll(j, Implies(And(i >= j, j >= 0), \
                                                           self.exactlyOnce(left, j+x)))))
                else:
                    return Exists(i, And(closeRangeForm(i, lb, ub), self.exactlyOnce(right, i+x), \
                                         Implies(i < 0, \
                                                 ForAll(j, \
                                                        Implies(And(i <= j, j <= 0), \
                                                                self.exactlyOnce(left, j+x)))),
                                         Implies(i >= 0, \
                                                 ForAll(j, \
                                                        Implies(And(i >= j, j >= 0), \
                                                                self.exactlyOnce(left, j+x))))))

            elif op == '->':
                assert False, "Not Implemented!"
                return True
                
        # un-wrap the Formula
        if type(formula) is Formula:

            res = self.exactlyOnce(formula[0], x)
            
            if enableQE:
                # eliminate quantifiers
                if type(formula[0]) is UnExp:
                    op = formula[0].operator
                    if op == 'G' or op == 'F':
                        res = qe(res).as_expr()

                elif type(formula[0]) is BinExp:
                    op = formula[0].operator
                    if op == 'U':
                        res = qe(res).as_expr()
                    
            return res
        
        if type(formula) is QuantExp:
            
            if not hasattr(formula, 'quant') or not hasattr(formula, 'freevars'):
                return self.exactlyOnce(formula.formula, x)
            else:
                if formula.quant == "Forall" or formula.quant == "Forall_sym" \
                   or formula.quant == "Forall_neq":
                    conjunct = []
                    rules = []

                    for var in formula.freevars:
                        assert var.prop in self.property_map, \
                            "Undefined type %s!" % var.prop
                    
                    # use * for splat operation
                    for tup in itertools.product(*[self.property_map[var.prop] \
                                                   for var in formula.freevars]):
                        rule = dict()
                        for i in range(0,len(formula.freevars)):
                            rule[formula.freevars[i].name] = tup[i]
                            rules.append(rule)
                
                    # reduce redundant rewriting rules
                    for r in self.ruleReduction(rules, formula.quant):
                        rw = SymbolRewriter(r)
                        f_copy = deepcopy(formula.formula)
                        rw.rewrite(f_copy)
                        conjunct.append(self.exactlyOnce(f_copy, x))
    
                    return And(conjunct)

                elif formula.quant == "Exists":
                    disjunct = []
                    rules = []

                    for var in formula.freevars:
                        assert var.prop in self.property_map, \
                            "Undefined type %s!" % var.prop
                    
                    # use * for splat operation
                    for tup in itertools.product(*[self.property_map[var.prop] \
                                                   for var in formula.freevars]):
                        rule = dict()
                        for i in range(0,len(formula.freevars)):
                            rule[formula.freevars[i].name] = tup[i]
                            rules.append(rule)

                    for r in rules:
                        rw = SymbolRewriter(r)
                        f_copy = deepcopy(formula.formula)
                        rw.rewrite(f_copy)
                        disjunct.append(self.exactlyOnce(f_copy, x))

                    return Or(disjunct)
                    
        return None
