# MTL Scheduler: a general purpose scheduling tool.
# Copyright (C) 2015  Yi Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from z3 import *
import itertools

# Utility functions

'''
InfType = Datatype('InfType')
InfType.declare('pInf')
InfType.declare('nInf')
InfType.declare('const')
InfType = InfType.create()
'''

'''
Extended Integer: int U (-oo, +oo)
Represented by a pair (inf:Bool, val:Int)
+oo: (true, v) where v > 0
-oo: (true, v) where v < 0
other integer v: (false, v)

(true, 0) is not allowed
+oo + -oo is undefined
'''

'''
class ExtendedInt:
    
    def __init__(self, inf, val):
        #assert (not val == 0) or (not inf), \
        #    "Undefined value: (%s, %s)!" % (inf, val)
        self.inf = inf
        self.val = val

    def __add__(self, other):
        other = _to_EInt(other)
        #assert not (self.inf and other.inf and self.val * other.val < 0), \
        #    "Undefined sum!"
        
        return ExtendedInt(Or(self.inf, other.inf), self.val + other.val)

    def __radd__(self, other):
        return self.__add__(other)

    def __eq__(self, other):
        other = _to_EInt(other)
        return Or(And(self.inf, other.inf), \
                  And(Not(self.inf), Not(other.inf), self.val == other.val))
    
    def __ne__(self, other):
        return Not(self.__eq__(other))

    def __lt__(self, other):
        other = _to_EInt(other)
        return And(Not(self.inf),
                   Or(other.inf, self.val < other.val))

    def __le__(self, other):
        return Or(self.__lt__(other), self.__eq__(other))
    
    def __gt__(self, other):
        return Not(self.__le__(other))

    def __ge__(self, other):
        return Not(self.__lt__(other))

def EInt(name):
    return ExtendedInt(Bool('%s.inf' % name), Int('%s.val' % name))

def EIntVal(v):
    return ExtendedInt(BoolVal(False), IntVal(v))

def pInf():
    return ExtendedInt(BoolVal(True), IntVal(1))

def nInf():
    return ExtendedInt(BoolVal(True), IntVal(-1))

def _to_EInt(v):
    if isinstance(v, ExtendedInt):
        return v
    elif is_int(v):
        return ExtendedInt(BoolVal(False), v)
    else:
        return EIntVal(v)
'''

def representInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False


pInf = 150
nInf = -150
    

def getRange(lb, ub):
    # print "convert: [%s, %s]" % (lb, ub)
    lbv, ubv = 1000, -1000
    
    if lb == '-oo':
        lbv = nInf 
    elif lb == '+oo':
        lbv = pInf
    else:
        lbv = int(lb)
        
    if ub == '-oo':
        ubv = nInf 
    elif ub == '+oo':
        ubv = pInf
    else:
        ubv = int(ub)
        
    assert lbv <= ubv, 'Range [%s, %s] is invalid!' % (lb, ub)

    return lbv, ubv


def closeRangeForm(x, lb, ub):
    
    if lb == '-oo':
        if ub == '+oo':
            return True
        else:
            return x <= int(ub)
    elif ub == '+oo':
        if lb == '-oo':
            return True
        else:
            return x >= int(lb)
    elif lb == '+oo' or ub == '-oo':
        assert False, 'Range [%s, %s] is invalid!' % (lb, ub)
        return None
    else:
        lbv = int(lb)
        ubv = int(ub)
        assert lbv <= ubv, 'Range [%s, %s] is invalid!' % (lb, ub)
        return And(lbv <= x, x <= ubv)

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = itertools.tee(iterable)
    next(b, None)
    return itertools.izip(a, b)
