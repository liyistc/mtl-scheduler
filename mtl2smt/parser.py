# MTL Scheduler: a general purpose scheduling tool.
# Copyright (C) 2015  Yi Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from __future__ import unicode_literals, print_function
from pypeg2 import *
import traceback
import sys
import codecs

# Simple parser for Metric Temporal Logic scheduling model.

r = re.compile

class BinOp(str):
    grammar = r(r'U|->|=>')

class UOp(str):
    grammar = r(r'\!|G|F')

class Index(str):
    grammar = r(r'\d+|-\d+|-oo|\+oo')

class Digit(str):
    grammar = r(r'\d+')
    
class Formula(List):
    def accept(self, visitor):
        visitor.visit(self)
        for child in self:
            child.accept(visitor)
    
class Boolean(Keyword):
    grammar = Enum(K("true"), K("false"))

class PrecSymbol(Keyword):
    grammar = Enum(K("Before"), K("After"), \
                   K("Between"), K("Currently"))

class Quantifier(Keyword):
    grammar = Enum(K("Forall"), K("Forall_sym"), K("Forall_neq"), K("Exists"))

Symbol.check_keywords = True
    
class Identifier(Symbol):
    pass

class FuncSymbol(Symbol):
    pass

class SimpleArgs(List):
    grammar = csl([Boolean, Digit, Identifier])

    def accept(self, visitor):
        visitor.visit(self)

class FormulaArgs(List):
    grammar = csl([Formula, Identifier])
    
    def accept(self, visitor):
        visitor.visit(self)
    
class Predicate(Plain):
    # start(x), end(x), Duration(x,10), P(x)
    grammar = attr("func", FuncSymbol), '(', attr("arg", SimpleArgs) ,')'

    def accept(self, visitor):
        visitor.visit(self)
        self.arg.accept(visitor)

class Precedence(Plain):
    # Before(phi), After(phi), Between(phi, psi), Currently(phi) 
    grammar = attr("func", PrecSymbol), '(', attr("arg", FormulaArgs), ')'

    def accept(self, visitor):
        visitor.visit(self)
        self.arg.accept(visitor)

class Interval(Plain):
    grammar = '[', attr('lb', Index), ',', attr('ub', Index), ']'

class NaryAnd(List):
    grammar = Formula, some('&', Formula)

    def accept(self, visitor):
        visitor.visit(self)
        for child in self:
            child.accept(visitor)

class NaryOr(List):
    grammar = Formula, some('|', Formula)
    
    def accept(self, visitor):
        visitor.visit(self)
        for child in self:
            child.accept(visitor)

class TypedVar(Plain):
    grammar = attr('name', Identifier), ':', attr('prop', Identifier)
    
    def accept(self, visitor):
        visitor.visit(self)

class QuantExp(Plain):
    # Forall x: X
    # Exists y: Y
    grammar = (attr('quant', Quantifier), \
               attr('freevars', csl(TypedVar)), '.', \
               attr('formula', Formula))

    def accept(self, visitor):
        visitor.visit(self)
        for var in self.freevars:
            var.accept(visitor)
        self.formula.accept(visitor)
    
# left recursion not supported
class BinExp(Plain):
    # U
    # &, |, ->
    grammar = [(attr('left', Formula), \
                attr('operator', BinOp), \
                attr('duration', Interval), \
                attr('right', Formula)), \
               (attr('left', Formula), \
                attr('operator', BinOp), \
                attr('right', Formula))]

    def accept(self, visitor):
        visitor.visit(self)
        self.left.accept(visitor)
        self.right.accept(visitor)

class UnExp(Plain):
    # G, F
    # !
    grammar = [(attr('operator', UOp), \
                attr('duration', Interval), \
                attr('operand', Formula)), \
               (attr('operator', UOp), \
                attr('operand', Formula))]

    def accept(self, visitor):
        visitor.visit(self)
        self.operand.accept(visitor)
    
Formula.grammar = [('(', NaryAnd, ')'), \
                   ('(', NaryOr, ')'), \
                   ('(', QuantExp, ')'), \
                   ('(', BinExp, ')'), \
                   UnExp, \
                   Boolean, \
                   Predicate, \
                   Precedence, \
                   ('(', Formula, ')')]
    
class TimeRange(Plain):
    grammar = K('begin'), attr('begin', Index), \
              K('final'), attr('final', Index)

    def accept(self, visitor):
        visitor.visit(self)

class Action(Plain):
    grammar = attr('items', csl(Identifier)), '=', attr('bound', Digit)
    
    def accept(self, visitor):
        visitor.visit(self)
        for item in self.items:
            item.accept(visitor)

class Actions(List):
    grammar = K('ACTIONS'), '{', \
              some(Action), \
              '}'
    
    def accept(self, visitor):
        visitor.visit(self)
        for child in self:
            child.accept(visitor)

class Property(Plain):
    grammar = attr('name', Identifier), ':', attr('items', csl(Identifier))
    
    def accept(self, visitor):
        visitor.visit(self)
        self.name.accept(visitor)
        for item in self.items:
            item.accept(visitor)

class Properties(List):
    grammar = K('PROPERTIES'), '{', \
              maybe_some(Property), \
              '}'
    
    def accept(self, visitor):
        visitor.visit(self)
        for child in self:
            child.accept(visitor)

class Constraints(List):
    grammar = K('CONSTRAINTS'), \
              some((Formula, ';'))
    
    def accept(self, visitor):
        visitor.visit(self)
        for child in self:
            child.accept(visitor)

class Model(List):
    grammar = TimeRange, Actions, Properties, Constraints
    
    def accept(self, visitor):
        visitor.visit(self)
        for child in self:
            child.accept(visitor)


def mtl_parse(string="", rule=None):
    ast = None
    try:
        print('Start parsing ' + string)
        ast = parse(string, Formula)
        print('Finish parsing')
    except:
        traceback.print_exc(file=sys.stdout)
        return None
    
    return ast

def mtl_parse_file(path="", rule=None):
    ast = None
    try:
        print('Start parsing ' + path)
        with codecs.open (path, 'r', 'utf_8') as source:
            print('Reading file ...')
            model = source.read()
            print('Parsing file ...')
            ast = parse(text=model, thing=Model, filename=path, \
                        whitespace=whitespace, comment=comment_sh)
        print('Finish parsing')
    except:
        traceback.print_exc(file=sys.stdout)
        return None

    return ast
