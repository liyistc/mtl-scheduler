# MTL Scheduler: a general purpose scheduling tool.
# Copyright (C) 2015  Yi Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from mtl2smt.converter import *
import sys
import argparse
import time

def main():
    parser = argparse.ArgumentParser(description = 'MTL Scheduler')
    parser.add_argument('model', metavar='MODEL', \
                        help = 'Path to the input model file')
    parser.add_argument('-p', '--parse', dest='parse', \
                        action='store_true', default=False, \
                        help = 'Parse the model only')
    parser.add_argument('-e', '--encode', dest='encode', \
                        action='store_true', default=False, \
                        help = 'Print SMT encoding')
    arg = parser.parse_args()
    
    print 'Analyzing input model file: ' + arg.model
    if os.path.exists(arg.model):
        compiler = MTLCompiler(arg.model, arg.encode)
    else:
        print 'Specified input file does not exist!'
        return

    if not arg.parse:
        start = time.time()
        print 'Solving instance ...'
        smt = compiler.compiles()
        end = time.time()
        print 'Finished solving!'
        print 'Time elapsed: %f (s)' % (end - start)

if __name__ == "__main__":
    sys.exit(main())
